
DIR=/afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/workspaces/mjj_05_09_2020
types="ws*mtx*root"

exec=/afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/scripts/wsexp

mkdir -p output log error
for file in $DIR/$types ; do 
    echo "Submitting for $file "
    b=`basename $file`
    bb=${b%.root}

    # create the executable
    echo "#!/bin/bash" >$bb.sh
    echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >> $bb.sh
    echo "alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'" >> $bb.sh
    echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" >> $bb.sh
    #    echo "setupATLAS" >> $bb.sh
    echo "asetup AnalysisBase,21.2.75,here" >> $bb.sh
    echo "lsetup \"root 6.08.06-HiggsComb-x86_64-slc6-gcc49-opt\" " >> $bb.sh
    echo "source /afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/scripts/setup.sh" >> $bb.sh
    echo "cd $DIR" >> $bb.sh
    echo "$exec $b" >> $bb.sh
    chmod +x $bb.sh
    #create the submit file
    echo "executable    = $DIR/$bb.sh" > $bb.sub
    echo "requirements = (OpSysAndVer =?= \"CentOS7\")" >> $bb.sub
#    echo "arguments             = $(ClusterId) $(ProcId)" >> $bb.sub
    echo "output                = $DIR/output/$bb.out" >> $bb.sub
    echo "error                 = $DIR/error/$bb.err" >> $bb.sub
    echo "log                   = $DIR/log/$bb.log" >> $bb.sub
    echo "+JobFlavour = \"workday\"" >> $bb.sub
    echo "MY.JobFlavour = \"workday\"" >> $bb.sub
    echo "queue" >> $bb.sub
    condor_submit $bb.sub
done 
            
