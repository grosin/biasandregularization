
DIR=/afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/workspaces/mjj_05_09_2020
types="ws*mtx*root"

exec=/afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/scripts/wsexp

nReplicas=500 
nToys=50 
here=`pwd`
mkdir -p output log error
for file in $DIR/$types ; do 
    echo "Submitting for $file "
    b=`basename $file`
    bb=${b%.root}
    mkdir -p $bb/output 
    mkdir -p $bb/log
    mkdir -p $bb/output
    for i in  $(seq 1 $nReplicas); do 
	# create the executable
	echo "#!/bin/bash" >  "$bb"/"$bb"_"$i".sh
	echo "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase" >>  "$bb"/"$bb"_"$i".sh
	echo "alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'" >> "$bb"/"$bb"_"$i".sh
	echo "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" >> "$bb"/"$bb"_"$i".sh
	#    echo "setupATLAS" >> $bb.sh
	echo "asetup AnalysisBase,21.2.75,here" >> "$bb"/"$bb"_"$i".sh
	echo "lsetup \"root 6.08.06-HiggsComb-x86_64-slc6-gcc49-opt\" " >> "$bb"/"$bb"_"$i".sh
	echo "source /afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/scripts/setup.sh" >> "$bb"/"$bb"_"$i".sh
	echo "cd $DIR/$bb" >> "$bb"/"$bb"_"$i".sh
	echo "$exec $DIR/$b -nToys $nToys -split toy_$i" >> "$bb"/"$bb"_"$i".sh
	chmod +x "$bb"/"$bb"_"$i".sh
    done  
    #create the submit file
    echo "executable    = $DIR/\$(type)" > $bb.sub
    echo "requirements = (OpSysAndVer =?= \"CentOS7\")" >> $bb.sub
#    echo "arguments             = $(ClusterId) $(ProcId)" >> $bb.sub
    echo "output                = $DIR/$bb/output/$bb_\$(type).out" >> $bb.sub
    echo "error                 = $DIR/$bb/error/$bb_\$(type).err" >> $bb.sub
    echo "log                   = $DIR/$bb/log/$bb_\$(type).log" >> $bb.sub
    echo "+JobFlavour = \"longlunch\"" >> $bb.sub
    echo "MY.JobFlavour = \"longlunch\"" >> $bb.sub
    echo "queue type matching $bb/*.sh" >> $bb.sub
#    echo "queue" >> $bb.sub
    condor_submit $bb.sub
done 
            
