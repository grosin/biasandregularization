#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
asetup AnalysisBase,21.2.75,here
lsetup "root 6.08.06-HiggsComb-x86_64-slc6-gcc49-opt" 
source /afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/scripts/setup.sh
cd /afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/workspaces/mjj_05_09_2020/ws_mtx_mjj
/afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/scripts/wsexp /afs/cern.ch/user/g/gabarone/work/Higgs/HiggsWW/Fit/histfitter/biasandreg/workspaces/mjj_05_09_2020/ws_mtx_mjj.root -nToys 50 -split toy_248
