std::vector<double> fitalvoloReg(double tau_value = 0, std::vector<float> scale = {}){
  TFile *_file0 = TFile::Open("ws_njet_expected.root");
  RooWorkspace *w = (RooWorkspace*) _file0->Get("myWS");
  RooStats::ModelConfig *mc = (RooStats::ModelConfig*) w->obj("ModelConfig");

  RooSimultaneous *simPdf = (RooSimultaneous*) mc->GetPdf();

  w->var("sigma_bin0_incl")->setVal( w->var("sigma_bin0_incl")->getVal()*scale.at(0) );
  w->var("sigma_bin1_incl")->setVal( w->var("sigma_bin1_incl")->getVal()*scale.at(1) );
  w->var("sigma_bin2_incl")->setVal( w->var("sigma_bin2_incl")->getVal()*scale.at(2) );
  w->var("sigma_bin3_incl")->setVal( w->var("sigma_bin3_incl")->getVal()*scale.at(3) );

  RooArgSet ww = w->allVars();
  RooDataSet *p_data = (RooDataSet*)RooStats::AsymptoticCalculator::MakeAsimovData(*mc, ww,ww);
  //RooAbsData *p_data =  w->data("asimovDataFullModel");

  p_data->Print();

  RooFIter iter = w->components().fwdIterator();

  const RooArgSet *pois=mc->GetParametersOfInterest();
  vector <RooRealVar*> *poisV=new vector <RooRealVar*>();
  vector <RooRealVar*> *binWidthV=new vector <RooRealVar*>();

  int nPois=0;
  TIter tp=pois->createIterator();
  RooRealVar *rtmp=nullptr;
  int lp=0;
  while((rtmp=(RooRealVar*)tp.Next())){
    if( (string(rtmp->GetName())).find("sigma")!=string::npos){
      poisV->push_back(rtmp);
      nPois++;
      RooRealVar *lvar=(RooRealVar*)w->var(Form("BinWidth_bin%d",lp));
      if(lvar!=nullptr){
        binWidthV->push_back(lvar);
        binWidthV->back()->Print();
      lp++;
      }
    }
  }

  
  RooAbsArg* arg;

  //while ((arg = iter.next())) {
  //      if (arg->IsA() == RooStarMomentMorph::Class()) {
  //              ((RooStarMomentMorph*)arg)->fixCache();
  //      }
  //}
  
  RooRealVar *tau=new RooRealVar("tau","tau",0);
  tau->setVal(tau_value);
  tau->setConstant(true);
  RooArgList *argsFormula=new RooArgList();
  //string formulaSum="(-tau * TMath::Sqrt(";
  string formulaSum="(tau * TMath::Exp(";
  argsFormula->add(*tau);
  /*
  string formulaSum="(-@4 *(";
  
  formulaSum += "((@2/0.430966-@1/0.951169) - (@1/0.951169-@0/1.85627)*(@1/0.951169-@0/1.85627))  +  ((@3/0.172987-@2/0.430966) - (@2/0.430966-@1/0.951169)*(@2/0.430966-@1/0.951169))";
  
  argsFormula->add(*w->var("sigma_bin0_incl"));
  argsFormula->add(*w->var("sigma_bin1_incl"));
  argsFormula->add(*w->var("sigma_bin2_incl"));
  argsFormula->add(*w->var("sigma_bin3_incl"));
  argsFormula->add(*tau);

  
  formulaSum+="))";
*/  



// here is the version with the cross section normalized to the expectation used.
  for(int k=1;k<nPois-1;k++){
    formulaSum+=Form("((-%s/%.3lf + 2*%s/%.3lf - %s/%.3lf)*(-%s/%.3lf + 2*%s/%.3lf -%s/%.3lf))",
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal(),
                     poisV->at(k+1)->GetName(),poisV->at(k+1)->getVal(),
                     poisV->at(k)->GetName(),poisV->at(k)->getVal(),
                     poisV->at(k-1)->GetName(),poisV->at(k-1)->getVal());
    
    if(k<nPois-2) formulaSum+=" + ";

    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
    argsFormula->add(*poisV->at(k+1));
    argsFormula->add(*poisV->at(k));
    argsFormula->add(*poisV->at(k-1));
  } 
  formulaSum+="))";
  
  argsFormula->Print();
  
  RooFormulaVar *regSum=new RooFormulaVar("regSum","regSum",formulaSum.c_str(),*argsFormula);
  regSum->Print();
  
  // Perform a fit
  RooAbsReal *Nll = simPdf->createNLL(*p_data);//
  RooAddition *NllMod=new RooAddition("nllSum","nullSum",RooArgList(*Nll,*regSum));

  
  RooMinimizer *minim = new RooMinimizer(*NllMod);
  
  minim->setEps(0.01);
  minim->optimizeConst(2);
  minim->setStrategy(0);
  minim->minimize("Minuit2");
  
  double best_fit = -2*NllMod->getVal();
  std:vector<double> vec_results;

  vec_results.push_back(tau_value);
  /*
  vec_results.push_back(w->var("sigma_bin0_incl")->getVal());
  vec_results.push_back(w->var("sigma_bin1_incl")->getVal());
  vec_results.push_back(w->var("sigma_bin2_incl")->getVal());
  vec_results.push_back(w->var("sigma_bin3_incl")->getVal());
  vec_results.push_back(w->var("MuqqZZ0")->getVal());
  vec_results.push_back(w->var("MuqqZZ1")->getVal());
  vec_results.push_back(w->var("MuqqZZ2")->getVal());
  */

  vec_results.push_back(w->var("sigma_bin0_incl")->getError());
  vec_results.push_back(w->var("sigma_bin1_incl")->getError());
  vec_results.push_back(w->var("sigma_bin2_incl")->getError());
  vec_results.push_back(w->var("sigma_bin3_incl")->getError());
  vec_results.push_back(w->var("MuqqZZ0")->getError());
  vec_results.push_back(w->var("MuqqZZ1")->getError());
  vec_results.push_back(w->var("MuqqZZ2")->getError());

 
  

  
  RooFormulaVar *regSumSM=new RooFormulaVar("regSum","regSum",formulaSum.c_str(),*argsFormula);
  regSum->Print();
  
  // Perform a fit
  RooAbsReal *NllSM = simPdf->createNLL(*p_data);//
  RooAddition *NllModSM=new RooAddition("nllSum","nullSum",RooArgList(*NllSM,*regSum));
  
  w->var("sigma_bin0_incl")->setVal(1.85627 ); w->var("sigma_bin0_incl")->setConstant(1);
  w->var("sigma_bin1_incl")->setVal(0.951169 ); w->var("sigma_bin1_incl")->setConstant(1);
  w->var("sigma_bin2_incl")->setVal(0.430966 ); w->var("sigma_bin2_incl")->setConstant(1);
  w->var("sigma_bin3_incl")->setVal(0.172987 ); w->var("sigma_bin3_incl")->setConstant(1);

  
  RooMinimizer *minimSM = new RooMinimizer(*NllModSM);
  
  
  minimSM->setEps(0.1);
  minimSM->optimizeConst(2);
  minimSM->setStrategy(2);
  minimSM->minimize("Minuit2");
  double best_fitSM = -2*NllModSM->getVal();

  vec_results.push_back(ROOT::Math::chisquared_cdf_c(abs(best_fitSM-best_fit), 4));

  return vec_results;
}


void scan(){
  std::vector<double> andrea;
  std::vector<float> scale = {0.9,1.05,1.50,1.30};

  TGraph g_sigma0;
  TGraph g_sigma1;
  TGraph g_sigma2;
  TGraph g_sigma3;
  TGraph g_muZZ0;
  TGraph g_muZZ1;
  TGraph g_muZZ2;

  
  TGraph g_pvalue;

  double matrix_index=0;
  for (unsigned int i=0; i<100; ++i ){
    double tau =0.05*i;
    andrea = fitalvoloReg(tau,scale);
    cout << andrea.at(0) << endl;
    cout << andrea.at(1) << endl;
    cout << andrea.at(2) << endl;
    cout << andrea.at(3) << endl;
    cout << andrea.at(4) << endl;
    cout << andrea.at(5) << endl;
    cout << andrea.at(6) << endl;
    cout << andrea.at(7) << endl;
    cout << andrea.at(8) << endl;
    g_sigma0.SetPoint(i, tau, andrea.at(1)/(1.85627*scale.at(0)) );
    g_sigma1.SetPoint(i, tau, andrea.at(2)/(0.951169*scale.at(1)) );
    g_sigma2.SetPoint(i, tau, andrea.at(3)/(0.430966*scale.at(2)) );
    g_sigma3.SetPoint(i, tau, andrea.at(4)/(0.172987*scale.at(3)) );
    
    g_muZZ0.SetPoint(i, tau, andrea.at(5) );
    g_muZZ1.SetPoint(i, tau, andrea.at(6) );
    g_muZZ2.SetPoint(i, tau, andrea.at(7) );
    g_pvalue.SetPoint(i, tau, andrea.at(8));

  }

  TLegend *legend = new TLegend(0.6,0.7,0.9,0.9);
  TCanvas *c = new TCanvas("c","c", 800,800);{
  c->cd();
  g_sigma0.GetYaxis()->SetRangeUser(0.0,1.5);
  g_sigma0.GetYaxis()->SetTitle("bias/p-value");
  g_sigma0.GetXaxis()->SetTitle("#tau");
  g_sigma0.SetLineColor(kRed);
  g_sigma1.SetLineColor(kBlue);
  g_sigma2.SetLineColor(kGreen+1);
  g_sigma3.SetLineColor(kMagenta);
  g_muZZ0.SetLineColor(kBlack);
  g_muZZ1.SetLineColor(kViolet+2);
  g_muZZ2.SetLineColor(kCyan);

  g_sigma0.Draw("apl");
  g_sigma1.Draw("l, same");
  g_sigma2.Draw("l, same");
  g_sigma3.Draw("l, same");

  g_muZZ0.Draw("l, same");
  g_muZZ1.Draw("l, same");
  g_muZZ2.Draw("l, same");
  g_pvalue.Draw("l, same");

 
  legend->AddEntry(&g_sigma0,"sigma bin 0","l");
  legend->AddEntry(&g_sigma1,"sigma bin 1","l");
  legend->AddEntry(&g_sigma2,"sigma bin 2","l");
  legend->AddEntry(&g_sigma3,"sigma bin 3","l");
  legend->AddEntry(&g_muZZ0,"mu ZZ bin 0","l");
  legend->AddEntry(&g_muZZ1,"mu ZZ bin 1","l");
  legend->AddEntry(&g_muZZ2,"mu ZZ bin 2-3","l");
  legend->AddEntry(&g_pvalue,"p-value (SM hypo)","l");

  legend->Draw();
  }
  c->SaveAs("a.eps");
  
}
