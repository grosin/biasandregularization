/*---------------------------------------------------------------------------------------

efftools.c

The purpose of this macro is to collect tools common to efficiency calculations.

To run this macro, ?? 

The functions include: 
  FUNCTION         | PURPOSE
  ----------------------------------------------------------------------------------------
  Get1DMatrixBinned| ?  
  g2               | A function: builds a gaussian of the form x^-0.5*(a^2+b^2)
  fun2             | A function: built out of g2 gaussians 
  SmartRebin       | Rebins a histogram to a new, specified binning 
  FiLLMatrix       | Fills the 2x2 matrix Pass/Fail Particle vs Pass/Fail Reco
  PlotMartrix      | Prints a latex table reporting the 2x2 matrix Pass/Fail Particle vs
                   | Pass/Fail Reco 
  GetEff           | Calculates the efficiency
  GetEffErr        | Calculates the error on the efficiency
  ReadListOfFiles  | Reads a list of files and prints out the filename, label, cross
                   | cross section, mass, and the up & down errors on the cross section

---------------------------------------------------------------------------------------*/

//#include <vector>
TH1F *Get1DMatrixBinned(TH3F *h, int bin){
  TH1F *toRet=NULL;//new TH1F("toRet","",10,0,10);
  
  //toRet=(TH1F*)h->ProjectionZ("_pz",bin,bin,0,h->GetYaxis()->GetNbins()+1);
  toRet=(TH1F*)h->ProjectionZ("_pz",bin,bin,0,-1);
  return toRet;
  
  /*
  toRet->SetBinContent(1,h->ProjectionZ("_pz",1,-1,1,-1)->GetBinContent(1));
  toRet->SetBinContent(2, h->ProjectionZ("_pz",1,-1,1,-1)->GetBinContent(2));
  
  toRet->SetBinContent(3,h->ProjectionZ("_pz",bin,bin,bin,bin)->GetBinContent(3));
  toRet->SetBinContent(4,h->ProjectionZ("_pz",bin,bin,1,-1)->GetBinContent(3),
                       //-toRet->GetBinContent(3)
                       +h->ProjectionZ("_pz",bin,bin+1,1,1,"e")->GetBinContent(4));
  
  toRet->SetBinContent(5,h->ProjectionZ("_pz",2,-1,bin,bin)->GetBinContent(3) 
                       //-toRet->GetBinContent(3)
                       +h->ProjectionZ("_pz",1,1,bin,bin)->GetBinContent(5));
  
  toRet->SetBinContent(6,h->ProjectionZ("_pz",1,-1,1,-1)->GetBinContent(6));
  */
  return  toRet;
}


Double_t g2(Double_t *x, Double_t *par) {
   Double_t r1 = Double_t((x[0]-par[1])/par[2]);
   Double_t r2 = Double_t((x[1]-par[3])/par[4]);
   return par[0]*TMath::Exp(-0.5*(r1*r1+r2*r2));
}

Double_t fun2(Double_t *x, Double_t *par) {
   Double_t *p1 = &par[0];
   Double_t *p2 = &par[5];
   Double_t *p3 = &par[10];
   Double_t result = g2(x,p1) + g2(x,p2) + g2(x,p3);
   return result;
}

TH2F *SmartRebin(TH2 *old_,vector <double> bins){
  cout<<"Rebinning "<<old_->GetName();
  TH2 *old=(TH2*)old_->Clone("old"); //the original histogram
  //create a new TH2 with your bin arrays spec
   TH2F *h = new TH2F( string(string(old_->GetName())+"rebin").c_str(),old->GetTitle(),bins.size()-1,&bins[0],bins.size()-1,&bins[0]);
   h->GetXaxis()->SetTitle(old->GetXaxis()->GetTitle());
   h->GetYaxis()->SetTitle(old->GetYaxis()->GetTitle());
   h->GetZaxis()->SetTitle(old->GetZaxis()->GetTitle());
  
   TAxis *xaxis = old_->GetXaxis();
   TAxis *yaxis = old_->GetYaxis();
   for (int j=1;j<=yaxis->GetNbins();j++) 
     for (int i=1;i<=xaxis->GetNbins();i++) 
       h->Fill(xaxis->GetBinCenter(i),yaxis->GetBinCenter(j),old->GetBinContent(i,j));
   
   delete old;
   cout<<" Will  return "<<h->GetName()<<endl;
   return h;
}

vector < vector <double>> FiLLMatrix(TH1F *hsemilep, TH1F *other, double &norm,double &acceptance,double &acceptanceE, TH1F *hall=NULL,TH1F *hallElse=NULL){
  
  char anyname[100];sprintf(anyname,"%s%s",hsemilep->GetName(),"_any");
  TH1F *any=(TH1F*)hsemilep->Clone(anyname);
  any->Add(other);
  sprintf(anyname,"%s%s",hsemilep->GetName(),"_else");
  TH1F *helsec=NULL;

  if(hall!=NULL) {
    hall->Add(hsemilep,-1);
  }
  
  vector < vector < double > > m;
  m.resize(2);
  m[0].resize(2); m[1].resize(2);
  
  if(hall==NULL){
    // Pass Reco  pass Particle 
    m[0][0]=hsemilep->GetBinContent(3);
    // Fail Reco  Pass Particle
    m[0][1]=hsemilep->GetBinContent(4);
    // Fail Particle Pass Reco 
    m[1][0]=hsemilep->GetBinContent(6);
    // Fail All 
    m[1][1]=hsemilep->GetBinContent(5);
  }
  else {

    // Pass Reco  pass Particle 
    //m[0][0]=hall->GetBinContent(3);
    // Fail Reco  Pass Particle
    //m[0][1]=hall->GetBinContent(4);
    // Fail Particle Pass Reco 
    //m[1][0]=hall->GetBinContent(6);
    // Fail All 
    //m[1][1]=hall->GetBinContent(5);
    
    //m[0][0]=0;
    //m[1][0]=hsemilep->GetBinContent(3)+hsemilep->GetBinContent(4);
    //m[0][1]=hall->GetBinContent(3)+hall->GetBinContent(4);
    //m[1][1]=0;
    
    m[0][0]=0;
    m[1][0]=hsemilep->GetBinContent(3);
    m[0][1]=hall->GetBinContent(4)+hsemilep->GetBinContent(4)+hsemilep->GetBinContent(3)+hall->GetBinContent(3);
    m[1][1]=hall->GetBinContent(5)+hsemilep->GetBinContent(5);
    
    /*
    // Pass Reco  pass Particle 
    m[0][0]=hsemilep->GetBinContent(3);
    // Fail Reco  Pass Particle
    m[0][1]=hsemilep->GetBinContent(4)+hall->GetBinContent(3)+hall->GetBinContent(4);
    // Fail Particle Pass Reco 
    m[1][0]=hsemilep->GetBinContent(6);
    // Fail All 
    m[1][1]=hall->GetBinContent(5)+hsemilep->GetBinContent(5);
    */
  }

  
  norm=m[0][0]+m[1][1]+m[1][0]+m[0][1];
  acceptance= (m[0][0]+m[0][1])/(norm);
  acceptanceE=acceptance/sqrt((m[0][0]+m[0][1]));
  
  return  m;
  
  /*
  
  char anyname[100];sprintf(anyname,"%s%s",hsemilep->GetName(),"_any");
  TH1F *any=(TH1F*)hsemilep->Clone(anyname);
  any->Add(other);
  
  vector < vector < double > > m;
  m.resize(2);
  m[0].resize(2); m[1].resize(2);
  // Pass Reco  pass Particle 
  m[0][0]=hsemilep->GetBinContent(3);
  // Fail Reco  Pass Particle
  m[0][1]=hsemilep->GetBinContent(4);
  // Fail Particle Pass Reco 
  m[1][0]=other->GetBinContent(3)+any->GetBinContent(6);
  // Fail All 
  m[1][1]=any->GetBinContent(5)+other->GetBinContent(4);

  vector < vector <double>> mS; mS.resize(2);
  mS[0].resize(2); mS[1].resize(2);
  // Pass Reco  pass Particle 
  mS[0][0]=hsemilep->GetBinContent(3);
  // Fail Reco  Pass Particle
  mS[0][1]=hsemilep->GetBinContent(4);
  // Fail Particle Pass Reco 
  mS[1][0]=hsemilep->GetBinContent(6);
  // Fail All 
  mS[1][1]=hsemilep->GetBinContent(5);
  
  norm=m[0][0]+m[1][1]+m[1][0]+m[0][1];

  //if(any->GetBinContent(1)!=0 && (m[0][0]+m[0][1])!=0){
  //acceptance= (m[0][0]+m[0][1])/norm;
  //acceptance= (m[0][0]+m[0][1])/hsemilep->GetBinContent(1);
  
  acceptance = (mS[0][0]+mS[0][1])/ ( mS[0][0]+mS[1][1]+mS[1][0]+mS[0][1]); 
  acceptanceE=acceptance/sqrt((mS[0][0]+mS[0][1]));
  //}
  //else {
  //acceptance=0;
  //acceptanceE=0;
  //}
  return m;
  */
  
}


void PlotMartrix(double norm, vector <vector <double>> m1,vector <vector <double>> m2){
    

if(norm==0){
    cout<<"\\left( \\begin{array}{c | c c}  "<<endl;
    cout<< "  -       &           \\text{Pass Reco}           &          \\text{Fail Reco} \\\\"<<endl;
    cout<<"\\hline"<<endl;
    cout<<setprecision(3)<<"\\text{Pass Particle} &"<< (int) m1[0][0]<<" \\ &  "<<(int) m1[0][1]<<"  \\\\"<<endl;
    
    cout<<setprecision(3)<<"\\text{Fail Particle} &" << (int) m1[1][0] << "\\  & " << (int) m1[1][1] <<" \\\\"<<endl;
    cout<<"\\end{array} \\right)"<<endl;
  }
  
 
  else {
    cout<<"\\left( \\begin{array}{c | c c}  "<<endl;
    cout<< "  -       &           \\text{Pass Reco}           &          \\text{Fail Reco} \\\\"<<endl;
    cout<<"\\hline"<<endl;
    cout<<setprecision(3)<<"\\text{Pass Particle} &"<<  m1[0][0]*100/norm<<" \\%   &  "<<m1[0][1]*100/norm<<" \\  \\\\"<<endl;
    
    cout<<setprecision(3)<<"\\text{Fail Particle} &" <<  m1[1][0]*100/norm << "\\%   & " << (int) m1[1][1]*100/norm <<" \\%  \\\\"<<endl;
    cout<<"\\end{array} \\right)"<<endl;
  }
																	    
}

double GetEff(vector <vector <double>> m,double norm=1){
  if(m[0][0]+m[0][1] !=0) 
    return (m[0][0]/norm+m[1][0]/norm)/(m[0][0]/norm+m[0][1]);
  else 
    return 0;
}

double GetEffErr(vector <vector <double>> m,double norm=1) {
  if(m[0][0]==0 ) return 0;
  return ( GetEff(m,norm)*1/sqrt(m[0][0]) );
}




void ReadListOfFiles( string list,vector <string> &files, vector <string> &labels,
		     vector <double> &xsec, vector <int> &mass,
                      vector <double> &xsec_eup, vector <double> &xsec_edown,vector <double> &weights ){
  files.clear(); labels.clear(); xsec.clear(); mass.clear();
  ifstream  listfile;


  listfile.open(list.c_str());
  while(!listfile.std::ifstream::eof()){
    char line[100000];
    char fnameI[1000];
    char titleI[1000];
    double isec=0;
    double imass=0;
    double isec_eU=0;
    double isec_eD=0;
    double weight=1;
    listfile.getline(line,1000);
    if(line[0]=='#') continue;
    if( !listfile) break;
    sscanf(line,"%s %s %lf %lf %lf %lf %lf[^\n]",&fnameI,&titleI,&isec,&imass,&isec_eU,&isec_eD,&weight);
    if(string(fnameI).size() > 0  ) { 
      files.push_back(fnameI);
      if(string(titleI).size()> 0) labels.push_back(titleI);
      else labels.push_back("");
      xsec.push_back(isec);
      mass.push_back(imass);
      xsec_eup.push_back(isec_eD*isec);
      xsec_edown.push_back(isec_eU*isec);
      weights.push_back(weight);
      cout<<"file "<<files.back()<<" label "<<labels.back()<<" xec "<<xsec.back()<<" mass "<<mass.back()<<" "<<xsec_eup.back()<<" "<<xsec_edown.back()<<" weight "<<weight<<endl;

    }
  }
}



void MakeTableOfModes(TH1F* base, TH1F **sub,vector<string> channels,double All=-1,double AllE=-1,
                      vector <double> inclusive=vector <double>(0), 
                      vector <double> inclusive_e=vector <double>(0)){
  
  bool condition= All!=-1 && AllE!=-1 && inclusive.size() > 0 && inclusive_e.size() > 0;
  
  for(int i =1; i<(int)base->GetNbinsX()+1; i++){
    cout<<RootToLatex(base->GetXaxis()->GetBinLabel(i));
    if(base->GetNbinsX() > 1 && i<(int)base->GetNbinsX())
      cout<<" & ";
  }
  if(condition) cout<<" & Inclusive ";
  cout<<" \\\\"<<endl;
  cout<<"All & ";
  
  for(int i =1; i<(int)base->GetNbinsX()+1; i++) {
    cout<<Form("%.2f",base->GetBinContent(i))<<" \\pm "<<Form("%.2f",base->GetBinError(i))<<" ";
    if( base->GetNbinsX() > 1 && i<(int)base->GetNbinsX())
      cout<<" & ";
  }
  if(condition) cout<<" & "<<Form("%.2f",All)<<" \\pm "<<Form("%.2f",AllE);
  cout<<" \\\\ "<<endl;
  
  for(int j=0; j<(int)channels.size(); j++){
    cout<<"$"<<RootToLatex(channels.at(j))<<"$ & ";
    for(int i =1; i<(int)sub[j]->GetNbinsX()+1; i++) {
      cout<<Form("%.2f",sub[j]->GetBinContent(i))<<" \\pm "<<Form("%.2f",sub[j]->GetBinError(i))<<" ";
      if( sub[j]->GetNbinsX() > 1 && i<(int)sub[j]->GetNbinsX())
      cout<<" & ";
    }
    if(condition) 
      { cout<<" & "<<Form("%.2f",inclusive.at(j))<<" \\pm "<<Form("%.2f",inclusive_e.at(j));  }   
    
    cout<<" \\\\ "<<endl;
  }

}
