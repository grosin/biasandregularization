export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
setupATLAS
asetup AnalysisBase,21.2.75,here
lsetup "root 6.08.06-HiggsComb-x86_64-slc6-gcc49-opt"
